# Retrieval robot
Making use of the library provided by jetbot, I have created a robot that detects the colors orange and white to performe a gathering robot. The idea for the robot is that there could be a storage-compartment that needs a set of crates placed in a "ready for pickup" area. During my simulating and testing i have used three diffrent orange dices as crates and a sheet of paper as the delivery surface.

# Video of failed and sucsessful runs with the robot
- Successfully collecting all dices: [Video](https://youtu.be/9f-TDL-VKHE)
- Failed collecting all dices and times out: [Video](https://youtu.be/j2QNNqOQtBM)
  

##  Prerequisite
- NVIDIA Jetson Nano
- [Jetank AI Kit](https://www.waveshare.com/jetank-ai-kit.htm)
- [JetBot SD card image](https://jetbot.org/v0.4.3/software_setup/sd_card.html)
- Video showing how to set up the jetbot [here](https://www.youtube.com/watch?v=qNy1hulFk6I)
- 32GB SD-Card or larger

## Installation
### 1. Assemble and set up the JETANK
Follow the [Aseembly Guides for Waveshare JETANK](https://www.youtube.com/watch?v=qNy1hulFk6I) up to 17.20 and get the tutorial examples working.

### 2. Expand memory of JetBot (optional)
The image provided by JetBot does not use the full memory card. You can expand the memory with the following commands:
```
sudo fdisk /dev/mmcblk0

Command (m for help): d
Partition number (1-14, default 14): 1

Command (m for help): n
Partition number (1,15-128, default 1): [Press enter]
First sector (34-123596766, default 28672): [Press enter]
Last sector, +sectors or +size{K,M,G,T,P} (28672-123596766, default 123596766): [Press enter]
Do you want to remove the signature? [Y]es/[N]o: N

Command (m for help): w 

sudo resize2fs /dev/mmcblk0p1

df -h
```

### 3. Clone this repository
```
$ git clone <this repo>
```

### 4. Move files to JETANK folder
The files are not dependent on each other, you can therefor move the desired file to run to the JETANK folder.

```
$ cd mv Jetson_Sorter.ipynb ../JETANK/
$ cd mv Jetson_Sorter.py ../JETANK/
$ cd mv colorTracking.ipynb ../JETANK/
```

## Usage
### Version 1.  Run the .py file from ternimal
```
$ cd JETANK
$ python3 Jetson_Sorter.py
```

### Version 2. Run the .ipynb from Jupyter Lab
#### 1. Go to Jupyter Lab
- Displayed on the LCD-screen of the JETANK you find the ip-adress for the JetBot. Go to \<ip-adress>::8888 and log in.

#### 2. Running the software
1. Locate the file Jetson_Sorter.ipynb in JETANK-folder.
2. Open the file
3. Edit the software to your needs and run each block of code
4. Start the robot with the last code-block and see the POV of the robot livestream on your computer.

# Change the program to your needs
Provided with the software is the colorTracking.ipynb. This software can be used to find what upper and lower HSV color values would be best to detected your preferred colors.

# Creator
Alexander Øvergård