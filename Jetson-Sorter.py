import traitlets
import ipywidgets
from IPython.display import display
from jetbot import Camera, bgr8_to_jpeg, Robot

import threading
import time
from SCSCtrl import TTLServo

import cv2
import numpy as np

###################################################
# setting up global variables
###################################################

robot = Robot()

# Get the camera feed
camera = Camera.instance(width=300, height=300)
image_widget = ipywidgets.Image()  # this width and height doesn't necessarily have to match the camera
camera_link = traitlets.dlink((camera, 'value'), (image_widget, 'value'), transform=bgr8_to_jpeg)

# Position the camera, Arm and gripper in standby position
TTLServo.servoAngleCtrl(5, 20, 1, 150)
TTLServo.servoAngleCtrl(4, 0, 1, 150)
TTLServo.servoAngleCtrl(1, -2, 1, 150)
TTLServo.xyInput(150, -60)

# Set starting state and restart-flags true
state = 0
restart_grab = True
restart_drop = True
restart_timer = True

# Values to find Orange Cube
orangeUpper = np.array([25, 255, 255])
orangeLower = np.array([ 0, 105, 110])

# Values to find White Area
whiteUpper = np.array([180, 40, 255])
whiteLower = np.array([0, 0, 185])

# This value desides how big the area we are travelig to will be
error_tor = 5

###################################################
# functions 
###################################################

# This is a subtitute for the time.sleep() so that the camera-feed wont frees when moving robot-arm
def gripDelay(n):
    timestart = time.time()
    while time.time()-timestart < n:
        yield False
        
# Servo arm opens claw and moves to a spot to pick up a cube
def grabItem():
    global restart_grab
    
    TTLServo.servoAngleCtrl(4, 0, 1, 150)
    yield from gripDelay(1)
    TTLServo.xyInput(120, -150)
    yield from gripDelay(2)
    TTLServo.servoAngleCtrl(4, -60, 1, 150)
    yield from gripDelay(2)
    TTLServo.xyInput(150, -50)
    yield from gripDelay(2)
    restart_grab = True
    yield True

# Servo arm moves arm in front of the robot and drops the item
def dropItem():
    global restart_drop
    
    TTLServo.xyInput(190, -160)
    yield from gripDelay(2)
    TTLServo.servoAngleCtrl(4, 0, 1, 150)
    yield from gripDelay(2)
    TTLServo.xyInput(150, -50)
    yield from gripDelay(2)
    
    restart_drop = True
    yield True

def searchTimer():
    global restart_timer
    
    yield from gripDelay(15)
    
    restart_timer = True
    yield True

# Returns center position of X and Y and radius of object
def getPosition(cnts):
    c = max(cnts, key=cv2.contourArea)
    ((box_x, box_y), radius) = cv2.minEnclosingCircle(c)
    X = int(box_x)
    Y = int(box_y)
    return radius, X, Y

# To avoid having the robot in a loop picking up cubes within the desired location
# this function removes the orange inside the white area to let the robot look for
# other cubes outside.
def removeOnArea(target, area):
    index = 0
    removed = 0
    radArea, xArea, yArea = getPosition(area)
    
    #for i in target:
    while index < len(target):
        ((box_x, box_y), radius) = cv2.minEnclosingCircle(target[index])
        X = int(box_x)
        Y = int(box_y)
        
        if X > (xArea + radArea) or X < (xArea - radArea):
            pass
        elif Y > (yArea + radArea) or Y < (yArea - radArea):
            pass
        else:
            target.pop(index)
            index -= 1
            removed += 1
        index +=1
    return removed

# detects desired colors and returns location of everything detected
def colorToFind(imageInput, colorLower, colorUpper):
    global state
    global grab

    cv2.rectangle(imageInput, (0,0), (300,75), (0,0,0), -1)
    
    hsv = cv2.cvtColor(imageInput, cv2.COLOR_BGR2HSV)
    mask = cv2.inRange(hsv, colorLower, colorUpper)
    mask = cv2.erode(mask, None, iterations=2)
    mask = cv2.dilate(mask, None, iterations=2)
    cnts = cv2.findContours(mask.copy(), cv2.RETR_EXTERNAL,
        cv2.CHAIN_APPROX_SIMPLE)[-2]
    center = None

    for c in cnts:  
        minRect = cv2.minAreaRect(c)
        box = cv2.boxPoints(minRect)
        box = np.intp(box)
        cv2.drawContours(imageInput, [box], 0, (0, 255, 0))
    return cnts

# Tracking function to drive the robot towards the desired target
# witht the version input you can cahnge where the robot wants to
# position the center of the biggest cube
def search(cnts, version):
    if version == 1:
        yPosition = 255
        xPosition = 130
    elif version == 2:
        yPosition = 200
        xPosition = 150
    
    # Find the contour of the largest area.
    c = max(cnts, key=cv2.contourArea)
    
    # Get the location of the center point of this area and the radius of this area.
    ((box_x, box_y), radius) = cv2.minEnclosingCircle(c)
    
    # X, Y are the center points of the area.
    X = int(box_x)
    Y = int(box_y)
    
    # get target alligned to desired y cordinates on camera
    if Y < yPosition - error_tor:
        robot.forward(0.4)
        ySet = False
    elif Y > yPosition + error_tor:
        robot.backward(0.4)
        ySet = False
    else:
        ySet = True

    # get target alligned to desired x cordinates on camera
    if X < xPosition - error_tor:
        robot.left(0.4)
        xSet = False
    elif X > xPosition + error_tor:
        xSet = False
        robot.right(0.4)
    else:
        xSet = True
    
    # when both x and y is in desired location return true and stop robot
    if xSet and ySet:
        robot.stop()
        return True
    
def verifyPickup(imageInput, colorLower, colorUpper):
    global state
    global grab

    cv2.rectangle(imageInput, (0,0), (300, 255-error_tor*5), (0,0,0), -1)
    cv2.rectangle(imageInput, (0,0), (130-error_tor*5, 300), (0,0,0), -1)
    cv2.rectangle(imageInput, (0,300), (300, 255+error_tor*5), (0,0,0), -1)
    cv2.rectangle(imageInput, (300,0), (130+error_tor*5, 300), (0,0,0), -1)
    
    hsv = cv2.cvtColor(imageInput, cv2.COLOR_BGR2HSV)
    mask = cv2.inRange(hsv, colorLower, colorUpper)
    mask = cv2.erode(mask, None, iterations=2)
    mask = cv2.dilate(mask, None, iterations=2)
    cnts = cv2.findContours(mask.copy(), cv2.RETR_EXTERNAL,
        cv2.CHAIN_APPROX_SIMPLE)[-2]
    center = None

    for c in cnts:  
        minRect = cv2.minAreaRect(c)
        box = cv2.boxPoints(minRect)
        box = np.intp(box)
        cv2.drawContours(imageInput, [box], 0, (0, 255, 0))
    return cnts

# set global variabels
drop = dropItem()
grab = grabItem()
timer = searchTimer()
  
###################################################
# Main loop
###################################################
def findColor(imageInput):    
    # including global variables
    global state
    global restart_grab
    global restart_drop
    global restart_timer
    global grab
    global drop
    global timer
    
    #detect the two colors we want to track
    orangeCnts = colorToFind(imageInput, orangeLower, orangeUpper)
    whiteCnts =  colorToFind(imageInput, whiteLower, whiteUpper)
    
    
    # State 0 ---------------------------------------------
    # In this state we are looking for the orange cube
    if state == 0:
        # remove orange object that is within a white object
        if len(orangeCnts) > 0 and len(whiteCnts) > 0:
            removed = removeOnArea(orangeCnts, whiteCnts)
            if removed >= 3:
                state = 4
                
        if restart_timer:
            timer = searchTimer()
            restart_timer = False
        if next(timer):
            state = 4
        
        if len(orangeCnts) < 1:
            robot.right(0.4)
        
        if len(orangeCnts) > 0:
            compleat = search(orangeCnts, 1)
            if compleat:
                restart_timer = True
                state = 1 
    
    # state 1 ---------------------------------------------
    # Grab item and validate it being picked up
    if state == 1:
        if restart_grab:
            grab = grabItem()
            restart_grab = False
        if next(grab):
            verify = verifyPickup(imageInput, orangeLower, orangeUpper)
            if len(verify) >= 1:
                state = 0
            else:
                state = 2
    
    # state 2 ---------------------------------------------
    # search for Area for drop off, and move to location
    if state == 2:
        if restart_timer:
            timer = searchTimer()
            restart_timer = False
        if next(timer):
            state = 4
        
        if len(whiteCnts) < 1:
            robot.right(0.4)
        if len(whiteCnts) > 0:       
            test = search(whiteCnts, 2)
            if test:
                restart_timer = True
                state = 3 
    
    # state 3 ---------------------------------------------
    # Drop item in drop off location
    if state == 3:            
        if restart_drop:
            drop = dropItem()
            restart_drop = False
        if next(drop):
            state = 0

    # state 4 ---------------------------------------------
    # When robot has found all pieces or timed out, stop the robot
    if state == 4:
        robot.stop()

    return imageInput

###################################################
# Execution Code
###################################################

def execute(change):
    global image_widget
    image = change['new']
    image_widget.value = bgr8_to_jpeg(findColor(image))

execute({'new': camera.value})
camera.unobserve_all()
camera.observe(execute, names='value')